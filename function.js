/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/

	//first function here:
	let printMyInformation = function myInformation(){
		alert("Hi! Please enter the needed information.");
		let myFullName = prompt("What is your full name?");
		let myAge = prompt("How old are you?");
		let myLocation = prompt("Where are you from?");

		console.log("Hello, " + myFullName);
		console.log("You are " + myAge + " " + "years old.");
		console.log("You live in " + myLocation);
	}
	printMyInformation();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/

	//second function here:
	function printMyFavoriteBands(){
		alert("Please enter your top 5 favorite bands/musical artists.");
		let myFirstFavoriteBandArtist = prompt("Enter your first favorite bands/musical artists:");
		let mySecondFavoriteBandArtist = prompt("Enter your second favorite bands/musical artists:");
		let myThirdFavoriteBandArtist = prompt("Enter your third favorite bands/musical artists:");
		let myFourthFavoriteBandArtist = prompt("Enter your fourth favorite bands/musical artists:");
		let myFifthFavoriteBandArtist = prompt("Enter your fifth favorite bands/musical artists:");

		console.log("1. " + myFirstFavoriteBandArtist);
		console.log("2. " + mySecondFavoriteBandArtist);
		console.log("3. " + myThirdFavoriteBandArtist);
		console.log("4. " + myFourthFavoriteBandArtist);
		console.log("5. " + myFifthFavoriteBandArtist);
	}
	printMyFavoriteBands();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/

	//third function here:
	function printMyFavoriteMovies(){
		alert("Enter your top 5 favorite movies of all time with your rating of each movie.");
		let myFirstFavoriteMovie = prompt("Enter your first favorite movie:");
		let myFirstFavoriteMovieRating = prompt("Enter your rating for this movie:");

		let mySecondFavoriteMovie = prompt("Enter your second favorite movie:");
		let mySecondFavoriteMovieRating = prompt("Enter your rating for this movie:");

		let myThirdFavoriteMovie = prompt("Enter your third favorite movie:");
		let myThirdFavoriteMovieRating = prompt("Enter your rating for this movie:");

		let myFourthFavoriteMovie = prompt("Enter your fourth favorite movie:");
		let myFourthFavoriteMovieRating = prompt("Enter your rating for this movie:");

		let myFifthFavoriteMovie = prompt("Enter your fifth favorite movie:");
		let myFifthFavoriteMovieRating = prompt("Enter your rating for this movie:");

		console.log("1. " + myFirstFavoriteMovie);
		console.log("Rotten Tomatoes Rating: " + myFirstFavoriteMovieRating + "%");

		console.log("2. " + mySecondFavoriteMovie);
		console.log("Rotten Tomatoes Rating: " + mySecondFavoriteMovieRating + "%");

		console.log("3. " + myThirdFavoriteMovie);
		console.log("Rotten Tomatoes Rating: " + myThirdFavoriteMovieRating + "%");

		console.log("4. " + myFourthFavoriteMovie);
		console.log("Rotten Tomatoes Rating: " + myFourthFavoriteMovieRating + "%");

		console.log("5. " + myFifthFavoriteMovie);
		console.log("Rotten Tomatoes Rating: " + myFifthFavoriteMovieRating + "%");
	}
	printMyFavoriteMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

	//fourth function here:
	// printUsers();
	let printFriends = function printUsers(){
		alert("Hi! Please add the names of your friends.");
		let friend1 = prompt("Enter your first friend's name:"); 
		let friend2 = prompt("Enter your second friend's name:"); 
		let friend3 = prompt("Enter your third friend's name:");

		console.log("You are friends with:")
		console.log(friend1); 
		console.log(friend2); 
		console.log(friend3); 
	};
	printFriends();
	// console.log(friend1);
	// console.log(friend2);